# Useful code for Wiki Conference Organizers


## Authors and acknowledgment
This code has been created by Silvia E. Gutiérrez De la Torre, Senior Program Officer for Libraries at the Wikimedia Foundation. If you want to contribute, do let me know!

## License
CC BY 4.0 

## Project status
I'm not currently developing this project but I'm open to suggestions and to accomadate them as time allows
